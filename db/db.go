//
// Copyright 2018  Pantacor Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//

package db

import (
	"context"
	"log"
	"time"

	"gitlab.com/pantacor/pantahub-base/utils"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	mgo "gopkg.in/mgo.v2"
)

// Session : MongoDb object
var Session *mgo.Database

// MongoDb : MongoDb Object
var MongoDb *mongo.Database

// MongoDbName : MongoDb Name
var MongoDbName string

// Connect : To connect to the mongoDb
func Connect() (*mgo.Database, *mongo.Database) {
	mongoDatabase := utils.GetEnv("MONGO_DB")

	session, err := GetMongoSession()
	if err != nil {
		log.Print("Panic1")
		panic(err)
	}
	//defer session.Close()
	Session = session.DB(mongoDatabase)
	//mongo-go-driver settings
	MongoClient, err := GetMongoClient()
	if err != nil {
		log.Print("Panic2")
		panic(err)
	}
	MongoDb = MongoClient.Database(mongoDatabase)

	return Session, MongoDb
}

// GetMongoSession : Get Mongo Session
func GetMongoSession() (*mgo.Session, error) {
	// XXX: make mongo host configurable through env
	MongoDbName = utils.GetEnv("MONGO_DB")
	mongoHost := utils.GetEnv("MONGO_HOST")
	mongoPort := utils.GetEnv("MONGO_PORT")
	mongoUser := utils.GetEnv("MONGO_USER")
	mongoPass := utils.GetEnv("MONGO_PASS")
	mongoRs := utils.GetEnv("MONGO_RS")

	mongoCreds := ""
	if mongoUser != "" {
		mongoCreds = mongoUser + ":" + mongoPass + "@"
	}

	mongoConnect := "mongodb://" + mongoCreds + mongoHost + ":" + mongoPort + "/" + MongoDbName

	if mongoRs != "" {
		mongoConnect = mongoConnect + "?replicaSet=" + mongoRs
	}
	log.Println("Will connect to mongo PROD db with: " + mongoConnect)

	return mgo.Dial(mongoConnect)
}

// GetMongoClient : To Get Mongo Client Object
func GetMongoClient() (*mongo.Client, error) {
	MongoDbName = utils.GetEnv("MONGO_DB")
	mongoHost := utils.GetEnv("MONGO_HOST")
	mongoPort := utils.GetEnv("MONGO_PORT")
	mongoUser := utils.GetEnv("MONGO_USER")
	mongoPass := utils.GetEnv("MONGO_PASS")
	mongoRs := utils.GetEnv("MONGO_RS")

	//Setting Client Options
	clientOptions := options.Client()

	credentials := options.Credential{
		AuthMechanism: "SCRAM-SHA-1",
		AuthSource:    MongoDbName,
		Username:      mongoUser,
		Password:      mongoPass,
		PasswordSet:   true,
	}
	mongoConnect := "mongodb://" + mongoHost + ":" + mongoPort
	clientOptions.SetAuth(credentials)
	clientOptions.SetReplicaSet(mongoRs)
	clientOptions.ApplyURI(mongoConnect)

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	MongoClient, err := mongo.Connect(ctx, clientOptions)
	log.Println("Will connect to mongo PROD db with: " + mongoConnect)

	return MongoClient, err
}
