
Pantahub GC APIs reference implementation.

# Prepare

 * get a reasonable fresh golang engine (1.9++) and install it
 * Install a mongodb database locally or get credentials for hosted instance

# Build

```
$ go get -v u gitlab.com/pantacor/pantahub-gc
...

$ go build -o ~/bin/pantahub-gc gitlab.com/pantacor/pantahub-gc
...
# RUn

```
$ source env.default
$ pantahub-gc
...
``` 
# Test

* Note:Make Sure testharness project is accessible

```
$ git clone -b develop https://gitlab.com/pantacor/pantahub-testharness
...

```
$ source env.default
$ cd tests
$ go test
...

# Run

```
$ pantahub-gc
mongodb connect: mongodb://localhost:27017/pantabase-gc
2017/06/19 21:56:04 Serving @ https://127.0.0.1:12366/
2017/06/19 21:56:04 Serving @ http://127.0.0.1:12365/
2017/06/19 21:56:04 Serving @ https://::1:12366/
2017/06/19 21:56:04 Serving @ http://::1:12365/
2017/06/19 21:56:04 Serving @ https://10.42.0.1:12366/
2017/06/19 21:56:04 Serving @ http://10.42.0.1:12365/
2017/06/19 21:56:04 Serving @ https://fe80::90a9:7a0:a5d5:f808:12366/
2017/06/19 21:56:04 Serving @ http://fe80::90a9:7a0:a5d5:f808:12365/
2017/06/19 21:56:04 Serving @ https://192.168.178.75:12366/
2017/06/19 21:56:04 Serving @ http://192.168.178.75:12365/
2017/06/19 21:56:04 Serving @ https://2a02:2028:66c:1201:3bae:315f:3ad8:c6ee:12366/
2017/06/19 21:56:04 Serving @ http://2a02:2028:66c:1201:3bae:315f:3ad8:c6ee:12365/
2017/06/19 21:56:04 Serving @ https://fe80::f64a:6b7d:ede:b208:12366/
2017/06/19 21:56:04 Serving @ http://fe80::f64a:6b7d:ede:b208:12365/
2017/06/19 21:56:04 Serving @ https://172.18.0.1:12366/
2017/06/19 21:56:04 Serving @ http://172.18.0.1:12365/
2017/06/19 21:56:04 Serving @ https://fe80::42:82ff:fea9:63a4:12366/
2017/06/19 21:56:04 Serving @ http://fe80::42:82ff:fea9:63a4:12365/
2017/06/19 21:56:04 Serving @ https://172.17.0.1:12366/
2017/06/19 21:56:04 Serving @ http://172.17.0.1:12365/
2017/06/19 21:56:04 Serving @ https://fe80::42:97ff:fef7:9daa:12366/
2017/06/19 21:56:04 Serving @ http://fe80::42:97ff:fef7:9daa:12365/
2017/06/19 21:56:04 Serving @ https://fe80::5491:a3ff:fed7:c798:12366/
2017/06/19 21:56:04 Serving @ http://fe80::5491:a3ff:fed7:c798:12365/
2017/06/19 21:56:04 Serving @ https://fe80::c0a3:b4ff:fe0d:e3b8:12366/
2017/06/19 21:56:04 Serving @ http://fe80::c0a3:b4ff:fe0d:e3b8:12365/
```

# Configure in Base API

We currently support the environment variables you can find in utils/env.go:

```
const (
	// Pantahub GC API end point
	ENV_PANTAHUB_GC_API: "http://localhost:2000",
)
```
